function calcResult() {
  let numFirst = +prompt('Введите первое число');
  let numSecond = +prompt('Введите второе число');
  let operator = prompt('Введите знак операции');
  switch (operator) {
    case '+':
      return numFirst + numSecond;
    case '-':
      return numFirst - numSecond;
    case '*':
      return numFirst * numSecond;
    case '/':
      if (numSecond ===0) {
        return('на 0 делить нельзя');
      } else {
        return numFirst / numSecond;
      };
  }
}

console.log(calcResult());

