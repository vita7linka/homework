function createNewUser() {
  const firstName = prompt("Введите Ваше имя");
  const lastName = prompt("Введите Вашу фамилию");
  const birthday = prompt("Введите дату Вашего рождения в формате dd.mn.year");

  let newUser = {
      name: firstName,
      surname: lastName,
      birthday: birthday,
      
      getLogin() {
      return this.name[0].toLowerCase() + this.surname.toLowerCase();
    },
    
    getPassword() {
      return (
        this.name[0].toUpperCase() +
        this.surname.toLowerCase() +
        this.birthday.slice(-4));
    },

    getAge() {
        let today = new Date();
        let year = this.birthday.slice(-4);
        let day = this.birthday.slice(0, 2);
        let month = this.birthday.slice(3, 5);
        let age = today.getFullYear() - year;
        if (
            month > today.getMonth() ||
            (month === today.getMonth() && day > today.getDay()))
            {
                age--;
            }
            return age;
        },
    };
    return newUser;
}

const res = createNewUser();
console.log(res.getAge());
console.log(res.getPassword());
